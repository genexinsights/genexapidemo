package models

import "sort"

type GenexAPIInterface interface {
}

type SurveyType struct {
	SurveyTypeID int    `json:"survey_type_id"`
	SurveyType   string `json:"survey_type"`
}

type Survey struct {
	SurveyID   int        `json:"survey_id"`
	SurveyName string     `json:"survey_name"`
	StartDate  string     `json:"start_date"`
	EndDate    string     `json:"end_date"`
	Active     int        `json:"active"`
	APIAccess  int        `json:"api_access"`
	Audit      int        `json:"audit"`
	Type       SurveyType `json:"type"`
	Questions  []Question `json:"questions"`
}

type ParticipantDemographicsResponse struct {
	DemographicsType  DemographicType `json:"demographic_type"`
	DemographicsValue string          `json:"demographic_value"`
}

type QuestionGroup struct {
	GroupText string       `json:"group_text"`
	GroupID   int          `json:"group_id"`
	Questions []QuestionQA `json:"questions"`
	Answers   []Answer     `json:"answers"`
}

type Question struct {
	QuestionNumber int            `json:"question_number"`
	QAID           int            `json:"qa_id"`
	Required       bool           `json:"required"`
	QuestionType   QuestionType   `json:"question_type"`
	QuestionFormat QuestionFormat `json:"question_format"`
	Matrix         bool           `json:"matrix"`
	QuestionGroup  QuestionGroup  `json:"question_group"`
}

type QuestionType struct {
	QuestionTypeID int    `json:"question_type_id"`
	QuestionType   string `json:"Radio Button"`
}
type QuestionFormat struct {
	QuestionFormatID int    `json:"question_format_id"`
	QuestionFormat   string `json:"question_format"`
}

type QuestionQA struct {
	QuestionText string `json:"question_text"`
	Column       int    `json:"column"`
	Group        int    `json:"group"`
}

type Answer struct {
	AnswerID    int    `json:"answer_id"`
	Answer      string `json:"answer_eng"`
	Order       int    `json:"order"`
	AnswerLabel string `json:"answer_ab"`
}

type DemographicType struct {
	DemographicTypeID int    `json:"demographic_types_id"`
	DemographicType   string `json:"demographic_type"`
	PlaceHolder       string `json:"placeholder"`
}

type Participant struct {
	ParticipantID int                       `json:"participant_id"`
	SurveyID      int                       `json:"survey_id"`
	Demographics  []ParticipantDemographics `json:"demographics"`
}

type ImportParticipant struct {
	ParticipantID int                       `json:"participant_id"`
	SurveyID      int                       `json:"survey_id"`
	Test          bool                      `json:"test"`
	Batch         bool                      `json:"batch"`
	BatchTotal    int                       `json:"batch_total"`
	BatchName     string                    `json:"batch_name"`
	Demographics  []ParticipantDemographics `json:"demographics"`
}

type ParticipantReposnse struct {
	ParticipantID int                               `json:"participant_id"`
	SurveyID      int                               `json:"survey_id"`
	SendDate      string                            `json:"send_date"`
	Sent          int                               `json:"sent"`
	Failed        int                               `json:"failed"`
	Replied       int                               `json:"replied"`
	Complete      int                               `json:"complete"`
	Test          int                               `json:"test"`
	ResolveID     int                               `json:"esc_sent"`
	RemoveRating  int                               `json:"remove_rating"`
	Anonymous     int                               `json:"anon"`
	Audit         int                               `json:"audit"`
	AuditRejected int                               `json:"audit_rejected"`
	Demographics  []ParticipantDemographicsResponse `json:"participant_demographics"`
	Replies       []ParticipantResults              `json:"replies"`
}

type ParticipantDemographics struct {
	DemographicsTypeID int    `json:"demographics_type_id"`
	DemographicsValue  string `json:"demographics_value"`
}

type ParticipantReply struct {
	QuestionID int    `json:"question_id"`
	AnswerID   int    `json:"answer_id"`
	TextAnswer string `json:"text_answer"`
	Column     int    `json:"column"`
	Group      int    `json:"group"`
}

type ParticipantResults struct {
	ReplyID    int            `json:"reply_id"`
	ReplyDate  string         `json:"reply_date"`
	QuestionID int            `json:"question_id"`
	Column     int            `json:"column"`
	Group      int            `json:"group"`
	Answer     Answer         `json:"answer"`
	FreeText   FreeTextResult `json:"free_text"`
}

type FreeTextResult struct {
	FreeTextID   int    `json:"freetext_id"`
	Text         string `json:"text"`
	Sentiment    string `json:"sentiment"`
	MainCategory string `json:"main_category"`
	SubCategory  string `json:"sub_category"`
}

func (r *ParticipantResults) GetQuestion(survey Survey) string {

	for _, v := range survey.Questions {

		if v.QAID == r.QuestionID {
			if v.Matrix {
				if r.Group < len(v.QuestionGroup.Questions) {
					return v.QuestionGroup.Questions[r.Group].QuestionText
				}
			} else {
				return v.QuestionGroup.Questions[0].QuestionText
			}
		}
	}

	return "unknown Question"

}

func (q *QuestionGroup) SortAnswers() {

	sort.Slice(q.Answers, func(i, j int) bool {
		if q.Answers[i].Order < q.Answers[j].Order {
			return true
		}
		if q.Answers[i].Order > q.Answers[j].Order {
			return false
		}
		return q.Answers[i].Order < q.Answers[j].Order
	})

}

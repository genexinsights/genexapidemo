package models

type ParticipantRepliesRequest struct {
	ParticipantID int    `json:"participant_id"`
	QuestionID    int    `json:"question_id"`
	SurveyID      int    `json:"surveys_id"`
	AnswerID      int    `json:"answer_id"`
	TextAnswer    string `json:"text_answer"`
	Column        int    `json:"column"`
	Group         int    `json:"group"`
}

type ParticipantRepliesBulkRequest struct {
	ParticipantID      int                `json:"participant_id"`
	SurveyID           int                `json:"survey_id"`
	ParticipantReplies []ParticipantReply `json:"participant_replies"`
}

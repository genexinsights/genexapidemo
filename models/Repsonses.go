package models

import "sort"

type ParticipantResponse struct {
	GenexAPIInterface
	ResultOK    bool                `json:"result_ok"`
	Errors      []string            `json:"errors"`
	Participant ParticipantReposnse `json:"participant"`
}

type ParticipantRepliesResponse struct {
	GenexAPIInterface
	ResultOK      bool                 `json:"result_ok"`
	Errors        []string             `json:"errors"`
	SurveyID      int                  `json:"survey_id"`
	ParticipantID int                  `json:"participant_id"`
	Replies       []ParticipantResults `json:"replies"`
}

type GenericResponse struct {
	GenexAPIInterface
	ResultOK bool     `json:"result_ok"`
	Errors   []string `json:"errors"`
}

type SubmitParticipantRepliesResponse struct {
	GenexAPIInterface
	ResultOK      bool     `json:"result_ok"`
	ResolveCaseID int      `json:"resolve_case_id"`
	Errors        []string `json:"errors"`
}

type ParticipantCreateResponse struct {
	GenexAPIInterface
	ResultOK      bool     `json:"result_ok"`
	Errors        []string `json:"errors"`
	ParticipantID int      `json:"participant_id"`
}

type ParticipantImportResponse struct {
	GenexAPIInterface
	ResultOK    bool              `json:"result_ok"`
	Errors      []string          `json:"errors"`
	Batch       bool              `json:"batch"`
	BatchTotal  int               `json:"batch_total"`
	BatchName   string            `json:"batch_name"`
	Participant ImportParticipant `json:"import_participant"`
}

type SurveyDemographicsResponse struct {
	GenexAPIInterface
	ResultOK     bool              `json:"result_ok"`
	Errors       []string          `json:"errors"`
	Demographics []DemographicType `json:"demographics"`
}

type SurveysListResponse struct {
	GenexAPIInterface
	ResultOK bool     `json:"result_ok"`
	Errors   []string `json:"errors"`
	Surveys  []Survey `json:"surveys"`
}

type SurveysResponse struct {
	GenexAPIInterface
	ResultOK bool     `json:"result_ok"`
	Errors   []string `json:"errors"`
	Survey   Survey   `json:"survey"`
}

func (s *SurveysListResponse) SortSurveys() {
	sort.Slice(s.Surveys, func(i, j int) bool {
		if s.Surveys[i].APIAccess > s.Surveys[j].APIAccess {
			return true
		}
		if s.Surveys[i].APIAccess < s.Surveys[j].APIAccess {
			return false
		}
		return s.Surveys[i].APIAccess < s.Surveys[j].APIAccess
	})
}

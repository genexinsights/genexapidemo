{{ template "master.tpl" . }}

{{ define "contentSection" }}

{{if .Errors}}

{{else}}
    <form class="form-horizontal" method="POST" action="/participant/create">
        <input type="hidden" name="surveyID" value="{{.surveyID}}" />

        {{range $key, $val := .Demographics}}
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">{{ $val.DemographicType }}</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="demographics_{{$val.DemographicTypeID}}" placeholder="">
                </div>
            </div>
        {{end}}

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Create Participant</button>
            </div>
        </div>
    </form>
{{end}}
   



   

{{end}}
{{ template "master.tpl" . }}

{{ define "contentSection" }}

{{if .Errors}}

{{else}}

<div class="panel panel-default">
            <div class="panel-heading">
                <div class="">
                  Surveys
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                The Survey list contains all surveys within a company. The `API Access` field indicates where a survey may have a participant and their replies added via API Calls
            </div>
        </div>



    <table class="table">
      <thead>
        <tr>
          <th> Survey Name </th>
          <th> Type </th>
          <th> Start Date </th>
          <th> End Date </th>
          <th> Import </th>
          <th> API Access </th>
          <th> Active </th>
        </tr>
      </thead>
      {{range $key, $val := .SurveyList.Surveys}}
        <tr>
          <td><a href="/survey/{{ $val.SurveyID }}"> {{ $val.SurveyName }} </a> </td>
          <td>  {{ $val.Type.SurveyType }} </td>
          <td> {{ $val.StartDate }} </td>
          <td> {{ $val.EndDate }} </td>
          <td><a class="btn btn-default btn-xs" href="/survey/import/{{ $val.SurveyID }}">Upload File</a></td>
          <td>
            {{if eq $val.APIAccess 1}}
                Yes
            {{else}}
                No
            {{end}}
          </td>
          <td> 
            {{if eq $val.Active 1}}
                Yes
            {{else}}
                No
            {{end}}
          </td>
        </tr>
      {{end}}
    </table>

 {{end}}   
{{end}}

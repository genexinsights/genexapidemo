{{ template "master.tpl" . }}

{{ define "contentSection" }}

{{if .Errors}}

{{else}}
      <div class="panel panel-default">
                <div class="panel-heading">
                        Participant ({{.Participant.ParticipantID}})
                </div>
                <div class="panel-body">

                     <div class="panel panel-default">
                        <div class="panel-heading">
                               Demographics
                        </div>
                        <div class="panel-body">        
                                <table class="table">
                                        <thead>
                                                <th>Type</th>
                                                <th>Value</th>
                                        </thead>
                                        <tbody>         
                                                {{range $i,$val := .Participant.Demographics}}
                                                        <tr>
                                                                <td>{{$val.DemographicsType.DemographicType}}</td>
                                                                <td>{{$val.DemographicsValue}}</td>
                                                        </tr>                                                        
                                                {{end}}
                                        </tbody>
                                
                                </table>
                        </div>
                     </div>

                     <div class="panel panel-default">
                        <div class="panel-heading">
                               Participant Replies
                        </div>
                        <div class="panel-body">   
                                {{range $key, $val := .Replies}}
            
                                        <div class="panel panel-default">
                                                <div class="panel-heading">
                                                        {{index $.QuestionMap $val.QuestionID}}
                                                </div>
                                                <div class="panel-body">
                                                {{if $val.Answer.AnswerID}}    
                                                        {{ $val.Answer.Answer }}
                                                {{else}}
                                                        {{ $val.FreeText.Text }}
                                                {{end}}

                                                </div>
                                        </div>
                                
                                {{end}}
                        </div>
                    </div>       
                   

                </div>
            </div>

{{end}}


{{end}}
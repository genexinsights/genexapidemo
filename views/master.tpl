<!DOCTYPE HTML>
<html lang="en">
<head>
    {{template "head.tpl"}}
</head>
<body>
    {{template "menu.tpl"}}

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

            {{if .Errors}}
                <BR/>
                <div class="alert alert-danger" role="alert">
                    {{range $key,$val := .Errors}}
                        <li>{{$val}}</li>
                    {{end}}
                </div>
            {{end}}  

            {{ block "contentSection" . }}{{ end }}
        </div>    
        <div class="col-md-1"></div>
    </div>    
</body>
</html>
{{ template "master.tpl" . }}

{{ define "contentSection" }}
    
    {{if .Errors}}

    {{else}}
        <BR />
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="pull-right">
                    <form action="/participant/new/survey/{{.Survey.Survey.SurveyID}}" >
                        <button class="btn btn-success">
                            Create Participant
                        </button>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                Below is the layout for the survey. To answer the survey, click 'Create Participant' button in the header.
            </div>
        </div>


            <form method="POST" action="">
                {{range $key, $question := .Survey.Survey.Questions}}

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Question {{ $question.QuestionNumber }}
                            {{if $question.Required}}
                                <span class="alert-danger"> *Required</span>   
                            {{end}}
                        </div>
                        <div class="panel-body">
                            {{if eq $question.QuestionType.QuestionTypeID 1}}
                                {{if $question.Matrix}}

                                        <h3>{{ $question.QuestionGroup.GroupText }}</h3>        


                                        <table class="table">
                                        <thead>
                                            <th>&nbsp</th>
                                            {{range $keyAns, $answer := $question.QuestionGroup.Answers}}
                                                <th>{{$answer.Answer}}</th>       
                                            {{end}}
                                        </thead>

                                        {{range $key, $questionQA := $question.QuestionGroup.Questions}}
                                            <TR>
                                            <td>{{$questionQA.QuestionText}}</td>
                                            
                                            {{range $keyAns, $answer := $question.QuestionGroup.Answers}}
                                                <td><input disabled="disabled" type="radio" name="question_{{$questionQA.Column}}_{{$question.QAID}}" value="{{$answer.AnswerID}}"></td>       
                                            {{end}}
                                            </TR>
                                        {{end}}
                                        </table>


                                    {{else}}                           
                                        {{range $key, $questionQA := $question.QuestionGroup.Questions}}
                                            <h3>{{$questionQA.QuestionText}}</h3>
                                            
                                            {{range $key, $answer := $question.QuestionGroup.Answers}}
                                                <input disabled="disabled" type="radio" name="question_{{$question.QAID}}" value="{{$answer.AnswerID}}"> {{$answer.Answer}}<BR/>       
                                            {{end}}
                                        {{end}}

                                    {{end}}

                                {{else if eq $question.QuestionType.QuestionTypeID 4}}
                        
                                    {{if eq $question.QuestionFormat.QuestionFormatID 3}}

                                            {{if $question.Matrix}}
                                                {{range $key, $questionQA := $question.QuestionGroup.Questions}}
                                                    <h3>{{$questionQA.QuestionText}}</h3>
                                                    <textarea disabled="disabled" name="question_{{$question.QAID}}" class="form-control"></textarea>
                                                {{end}}
                                            {{else}}
                                                    Question is not Currently Supported by this API Demo
                                            {{end}}

                                    {{else}}
                                        Question is not Currently Supported by this API Demo
                                    {{end}}
                                {{else}}    
                                    Other 
                                {{end}}

                            
                        </div>
                    </div>
                {{end}}

        
            </form>
    {{end}}
{{end}}
{{ template "master.tpl" . }}

{{ define "contentSection" }}


    {{if .Errors}}

    {{else}}

        {{range $key, $val := .Replies}}
            
                     
            <div class="panel panel-default">
                <div class="panel-heading">
                        {{index $.QuestionMap $val.QuestionID}}
                </div>
                <div class="panel-body">
                    {{if $val.Answer.AnswerID}}    
                        {{ $val.Answer.Answer }}
                    {{else}}
                        {{ $val.FreeText.Text }}
                    {{end}}

                </div>
            </div>

           
        {{end}}

    {{end}}


    <form method="GET" action="/participant/{{.ParticipantID}}">
        <button class="btn btn-info">View All Participant Data</button>
    <form>    

{{end}}
{{ template "master.tpl" . }}

{{ define "contentSection" }}

    {{if .Errors}}

    {{else}}

        <div>Import File: {{.FileName}}</div>

        <form action="/survey/import/{{.surveyID}}/processfile" method="POST">
            <input type="hidden" name="fileName" value="{{.FileName}}"/>
            <input type="hidden" name="surveyID" value="{{.surveyID}}"/>

            <table class="table">
                <tbody>
                    <tr>
                        <th>Batch File</th>
                        <th> <input type="checkbox" name="batch" /> </th>
                    </tr>

                    {{range $key, $val := .FileHeaders}}
                        <tr>
                            <th>{{ $val }} </th>
                            <th>
                                <select name="header_{{$key}}">
                                    {{range $k, $v := $.Demographics}}
                                            <option value="{{$v.DemographicTypeID}}">{{$v.DemographicType}}</option>
                                    {{end}}
                                </select>
                            </th>
                        </tr>
                    {{end}}
                </tbody>

            </table>
            <button type="submit" class="btn btn-default">Submit</button>

        </form>
    {{end}}
{{end}}

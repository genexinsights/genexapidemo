{{ template "master.tpl" . }}

{{ define "contentSection" }}

    {{if .Errors}}

    {{else}}  
        <h3>Thank You for completing this survey.</h3>
    {{end}}

    {{if gt .ResolveCaseID 0}}
       <div class="well well-warning">     
            We are sorry you have had a negative experience. An agent will contact you shortly. Your Case number is: {{.ResolveCaseID}} 
        </div>
    {{end}}


    <form method="GET" action="/participant/results/{{.ParticipantID}}">
            <button type="submit" class="btn btn-info">View Results</button>
    </form>
{{end}}
{{ template "master.tpl" . }}

{{ define "contentSection" }}


<table class="table">

    <thead>
        <tr>
        <th></th> 
        {{range $key, $val := .Demographics}}
             <th>{{$val.DemographicType}}</th>   
        {{end}}
        <th>Status</th> 
        </tr>
    </thead>

    {{range $key, $val := .ImportStatus}}
        <tr class="alert-success">
            <td>
                {{$key}}
            </td>
              {{range $k, $v := $.Demographics}}
                    {{range $dID, $demo := $val.Participant.Demographics}}
                        {{if eq $demo.DemographicsTypeID $v.DemographicTypeID}}
                            <th>{{$demo.DemographicsValue}}</th>
                        {{end}}
                    {{end}}   
              {{end}}
            <th>Success</th>  
        </tr>
    {{end}}

</table>

{{end}}
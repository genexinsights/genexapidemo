{{ template "master.tpl" . }}

{{ define "contentSection" }}


 {{if .Errors}}

 {{else}}

    <form method="POST" action="/survey/submit">
        <input type="hidden" name="participantID" value="{{.ParticipantID}}" />
        <input type="hidden" name="surveyID" value="{{.SurveyID}}" />

                {{range $key, $question := .Survey.Survey.Questions}}

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Question {{ $question.QuestionNumber }}
                            {{if $question.Required}}
                                <span class="alert-danger"> *Required</span>   
                            {{end}}
                        </div>
                        <div class="panel-body">
                            {{if eq $question.QuestionType.QuestionTypeID 1}}
                                {{if $question.Matrix}}

                                        <h3>{{ $question.QuestionGroup.GroupText }}</h3>        


                                        <table class="table">
                                        <thead>
                                            <th>&nbsp</th>
                                            {{range $keyAns, $answer := $question.QuestionGroup.Answers}}
                                                <th>{{$answer.Answer}}</th>       
                                            {{end}}
                                        </thead>

                                        {{range $key, $questionQA := $question.QuestionGroup.Questions}}
                                            <TR>
                                            <td>{{$questionQA.QuestionText}}</td>
                                            
                                            {{range $keyAns, $answer := $question.QuestionGroup.Answers}}
                                                <td><input type="radio" name="question_{{$question.QuestionType.QuestionTypeID}}_{{$question.QAID}}_{{$questionQA.Column}}_{{$questionQA.Group}}" value="{{$answer.AnswerID}}"></td>       
                                            {{end}}
                                            </TR>
                                        {{end}}
                                        </table>


                                    {{else}}                           
                                        {{range $key, $questionQA := $question.QuestionGroup.Questions}}
                                            <h3>{{$questionQA.QuestionText}}</h3>
                                            
                                            {{range $key, $answer := $question.QuestionGroup.Answers}}
                                                <input type="radio" name="question_{{$question.QuestionType.QuestionTypeID}}_{{$question.QAID}}_{{$questionQA.Column}}_{{$questionQA.Group}}" value="{{$answer.AnswerID}}"> {{$answer.Answer}}<BR/>       
                                            {{end}}
                                        {{end}}

                                    {{end}}

                                {{else if eq $question.QuestionType.QuestionTypeID 4}}
                        
                                    {{if eq $question.QuestionFormat.QuestionFormatID 3}}

                                            {{if $question.Matrix}}
                                                {{range $key, $questionQA := $question.QuestionGroup.Questions}}
                                                    <h3>{{$questionQA.QuestionText}}</h3>
                                                    <textarea name="question_{{$question.QuestionType.QuestionTypeID}}_{{$question.QAID}}_{{$questionQA.Column}}_{{$questionQA.Group}}" class="form-control"></textarea>
                                                {{end}}
                                            {{else}}
                                                    Question is not Currently Supported by this API Demo
                                            {{end}}

                                    {{else}}
                                        Question is not Currently Supported by this API Demo
                                    {{end}}
                                {{else}}    
                                    Other 
                                {{end}}

                            
                        </div>
                    </div>
                {{end}}

                <button type="submit" class="btn btn-success pull-right">Submit</button>
            </form>
 {{end}}
{{end}}
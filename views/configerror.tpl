{{ template "master.tpl" . }}

{{ define "contentSection" }}

    <div class="alert alert-danger" role="alert">
        Configuration Error: Please ensure, you have set the `apitoken`  and `apiendpoint` settings in your app.conf file.<BR><BR>
        If you are you using the docker demo, you can link the app.conf using -v app.conf:/go/src/genexAPISurvey/conf/app.conf. 
        Ensure that you using the app.conf template.                

    </div>

{{end}}
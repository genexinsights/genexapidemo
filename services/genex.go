package services

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"genexAPIDemo/models"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/astaxie/beego"
)

type Genex struct {
	ApiToken string
}

func (g *Genex) GetSurveys() (models.SurveysListResponse, error) {

	parameters := map[string]string{}
	res := g.DoRequest("surveys", parameters, http.MethodGet, nil)
	defer res.Body.Close()
	log.Println(res.StatusCode)
	if res.StatusCode == http.StatusOK {

		var target models.SurveysListResponse
		if err := json.NewDecoder(res.Body).Decode(&target); err != nil {
			log.Fatal(err)
		}

		return target, nil
	}

	return models.SurveysListResponse{}, errors.New("Failed to Get Surveys")

}

func (g *Genex) GetSurveyByID(id int) (models.SurveysResponse, error) {

	parameters := map[string]string{}
	res := g.DoRequest("survey/"+strconv.Itoa(id), parameters, http.MethodGet, nil)
	defer res.Body.Close()
	if res.StatusCode == http.StatusOK {

		var target models.SurveysResponse
		if err := json.NewDecoder(res.Body).Decode(&target); err != nil {
			log.Fatal(err)
		}

		return target, nil
	}

	return models.SurveysResponse{}, errors.New("Failed to Get Surveys")

}

func (g *Genex) GetSurveyDemographics(id int) (models.SurveyDemographicsResponse, error) {

	parameters := map[string]string{}
	res := g.DoRequest("survey/"+strconv.Itoa(id)+"/demographics", parameters, http.MethodGet, nil)
	defer res.Body.Close()
	if res.StatusCode == http.StatusOK {

		var target models.SurveyDemographicsResponse
		if err := json.NewDecoder(res.Body).Decode(&target); err != nil {
			log.Fatal(err)
		}

		return target, nil
	}

	return models.SurveyDemographicsResponse{}, errors.New("Failed to Get Surveys")

}

func (g *Genex) CreateParticipant(body interface{}) (models.ParticipantCreateResponse, error) {

	parameters := map[string]string{}
	res := g.DoRequest("participant", parameters, http.MethodPost, body)

	defer res.Body.Close()
	if res.StatusCode == http.StatusOK || res.StatusCode == 400 {

		var target models.ParticipantCreateResponse
		if err := json.NewDecoder(res.Body).Decode(&target); err != nil {
			log.Fatal(err)
		}
		return target, nil
	}

	return models.ParticipantCreateResponse{}, errors.New("An Internal Error Occured trying to create your participant")

}

func (g *Genex) ImportParticipant(body interface{}) (models.ParticipantImportResponse, error) {

	parameters := map[string]string{}
	res := g.DoRequest("participant/import", parameters, http.MethodPost, body)

	defer res.Body.Close()
	if res.StatusCode == http.StatusOK || res.StatusCode == 400 {

		var target models.ParticipantImportResponse
		if err := json.NewDecoder(res.Body).Decode(&target); err != nil {
			log.Fatal(err)
		}
		return target, nil
	} else {
		fmt.Println("HTTP STATUS =>", res.StatusCode)
	}

	return models.ParticipantImportResponse{}, errors.New("An Internal Error Occured trying to create your participant")

}

func (g *Genex) SubmitParticipantReplies(body interface{}) (models.SubmitParticipantRepliesResponse, error) {

	parameters := map[string]string{}
	res := g.DoRequest("replies", parameters, http.MethodPost, body)
	defer res.Body.Close()
	if res.StatusCode == http.StatusOK || res.StatusCode == 400 {

		var target models.SubmitParticipantRepliesResponse
		if err := json.NewDecoder(res.Body).Decode(&target); err != nil {
			log.Fatal(err)
		}
		fmt.Println(target)
		return target, nil
	} else {
		return models.SubmitParticipantRepliesResponse{}, errors.New("An error occured trying to store the replies")
	}
}

func (g *Genex) ViewParticipantResults(id int) (models.ParticipantRepliesResponse, error) {
	parameters := map[string]string{}
	res := g.DoRequest("participant/"+strconv.Itoa(id)+"/replies", parameters, http.MethodGet, nil)
	defer res.Body.Close()
	if res.StatusCode == http.StatusOK || res.StatusCode == 400 {
		var target models.ParticipantRepliesResponse
		if err := json.NewDecoder(res.Body).Decode(&target); err != nil {
			log.Fatal(err)
		}
		return target, nil
	}

	return models.ParticipantRepliesResponse{}, errors.New("An Internal Error Occured trying to fetch results")

}

func (g *Genex) ViewParticipant(id int) (models.ParticipantResponse, error) {

	parameters := map[string]string{}
	res := g.DoRequest("participant/"+strconv.Itoa(id), parameters, http.MethodGet, nil)

	defer res.Body.Close()
	if res.StatusCode == http.StatusOK || res.StatusCode == 400 {
		var target models.ParticipantResponse
		if err := json.NewDecoder(res.Body).Decode(&target); err != nil {
			log.Fatal(err)
		}
		return target, nil
	}

	return models.ParticipantResponse{}, errors.New("An Internal Error Occured trying participant")

}

func (g *Genex) DoRequest(method string, parameters map[string]string, httpMethod string, body interface{}) *http.Response {

	url := beego.AppConfig.String("apiendpoint") + method

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}

	client := http.Client{
		Transport: tr,
		Timeout:   time.Second * 10,
	}

	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			log.Fatal("Error Trying to encode JSON")
		}
	}

	req, err := http.NewRequest(httpMethod, url, buf)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "Genex API Demo App")
	req.Header.Set("Authorization", beego.AppConfig.String("apitoken"))
	q := req.URL.Query()
	//q.Add()
	for k, p := range parameters {
		q.Add(k, p)
	}

	req.URL.RawQuery = q.Encode()

	fmt.Println("Calling Enpoint: " + url)
	res, getErr := client.Do(req)

	if getErr != nil {
		log.Fatal(getErr)
	}

	return res
}

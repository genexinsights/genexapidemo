# Genex Insights API Demo

[![N|Solid](http://genex.co.za/wp-content/uploads/2015/10/genex-logo-n.png)](https://www.genex.co.za)

This Golong package is a working demo application for interacting with the Genex Insights API. The demo shows how to interpret the data coming back and how to build the layouts from the documention. 

The demo builds off the Beego MVC Framework. [https://www.beego.me]

For easy of use, the demo does not currently make use of any js components other than bootstrap.

All API interactions are called through the `genex.go` file. 

# Running the Demo

Make sure that you have the beego components installed.
```sh
$ go get -u github.com/astaxie/beego
$ go get -u github.com/beego/bee
```

Make Sure you have set your $GOPATH variable and added `$GOPATH/bin/` to your $PATH variable. You can get more information about setting up beego on their site [http://www.beego.me]

Cloning the API Repo,

```sh
$ git clone git@bitbucket.org:genexinsights/genexapidemo.git $GOPATH/src/genexAPIDemo
```

Navigate to your API demo directory and execute `bee run`
```sh
$ $GOPATH/src/genexAPIDemo
$ bee run
```

# Docker

Running the API Demo in docker.
```sh
docker run -ti --rm --name genexapi -p 8080:8080 -v ~/app.conf:/go/src/genexAPIDemo/conf/app.conf genexinsights/genexapidemo
```
You will need to mount in a config file containing the `apitoken` and `apiendpoint` parameters. The config file template looks like this.
```sh
appname = genexAPIDemo
httpport = 8080
runmode = dev
copyrequestbody = true
apitoken = ""
apiendpoint = ""
```

# Question Types

The demo currently supports the following question types

  - Radio Button (Single and Matrix)
  - Dropdown (FreeText Type Questions Only)

Rendering the display is up to the developer, in order to suit your individual display needs. Below is the basic rendering logic based on the API as to how Genex implements the display rendering. The most important requirement is to ensure when submitting your replies to genex that you pass through the question_id, group and column associated with the question answered.

# Radio Button (Non Matrix: Matrix = false)
```sh
{
    "question_number": 1,
    "qa_id": 471,
    "required": true,
    "question_type": {
        "question_type_id": 1,
        "question_type": "Radio Button"
    },
    "question_format": {
        "question_format_id": 2,
        "question_format": "non-numeric"
    },
    "matrix": false,
    "questions": null,
    "question_group": {
        "group_text": "",
        "group_id": 0,
        "questions": [
            {
                "question_text": "Please select an answer below",
                "group": 1,
                "column": 0
            }
        ],
        "answers": [
            {
                "answer_id": 1,
                "answer_eng": "Answer 1",
                "answer_ab": "Answer 1 Label",
                "order": 1
            },
            {
                "answer_id": 2,
                "answer_eng": "Answer 2",
                "answer_ab": "Answer 2 Label",
                "order": 2
            }
        ]
    }
}
```

The above response will render as follows:

Please select an answer below
- Answer 1 [Radio button]
- Answer 2 [Radio button]

# Radio Button (Non Matrix: Matrix = true)
```sh
 {
    "question_number": 1,
    "qa_id": 2,
    "required": true,
    "question_type": {
        "question_type_id": 1,
        "question_type": "Radio Button"
    },
    "question_format": {
        "question_format_id": 1,
        "question_format": "numeric"
    },
    "matrix": true,
    "question_group": {
        "group_text": "Please answer the following:",
        "group_id": 615,
        "answers": [
            {
                "answer_id": 3,
                "answer_eng": "Yes",
                "answer_ab": "Yes",
                "order": 1
            },
            {
                "answer_id": 4,
                "answer_eng": "No",
                "answer_ab": "No",
                "order": 2
            },
        ],
        "questions": [
            {
                "question_text": "Do you like apples?",
                "group": 1,
                "column": 0
            },
            {
                "question_text": "Do you like Pears?",
                "group": 2,
                "column": 0
            }
        ]
    }
}
```
The above will render as follows:

Please answer the following

|  | Yes | No |
| ------ | ------ | ------ |
| Do you like apples? | Radio Button | Radio Button |
| Do you like pears | Radio Button | Radio Button |


# Dropdown (FreeText) 
```sh
{
    "question_number": 2,
    "qa_id": 3,
    "required": true,
    "question_type": {
        "question_type_id": 4,
        "question_type": "Dropdown Menu"
    },
    "question_format": {
        "question_format_id": 3,
        "question_format": "freetext"
    },
    "matrix": true,
    "question_group": {
        "group_text": "Please answer the following:",
        "group_id": 621,
        "answers": [],
        "questions": [
            {
                "question_text": "Please provide any further comments or suggestions that you may have",
                "group": 1,
                "column": 1
            }
        ]
    }
}
```

The Above will render as follows:

Please provide any further comments or suggestions that you may have
[TEXT BOX/TEXT AREA]

### Todos

 - Dropdown - Select Questions
 - Checkbox Questions
 - Conditional (Skip) Logic



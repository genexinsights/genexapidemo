package routers

import (
	"genexAPIDemo/controllers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
)

func init() {

	var FilterTextConfig = func(ctx *context.Context) {
		if beego.AppConfig.String("apiendpoint") == "" || beego.AppConfig.String("apitoken") == "" {
			ctx.Redirect(302, "/configerror")
		}

	}
	beego.InsertFilter("/survey/", beego.BeforeRouter, FilterTextConfig)
	beego.InsertFilter("/participant/", beego.BeforeRouter, FilterTextConfig)

	beego.Router("/configerror", &controllers.MainController{}, "get:ConfigError")

	beego.Router("/", &controllers.MainController{}, "get:Welcome")
	beego.Router("/survey", &controllers.MainController{}, "get:Get")
	beego.Router("/survey/import/:id", &controllers.MainController{}, "get:Import")
	beego.Router("/survey/import/:id/upload", &controllers.MainController{}, "post:ImportUploadFile")
	beego.Router("/survey/import/:id/processfile", &controllers.MainController{}, "post:ImportUploadProcessFile")
	beego.Router("/survey/:id", &controllers.MainController{}, "get:ViewSurvey")
	beego.Router("/survey/submit", &controllers.MainController{}, "post:SubmitSurvey")

	beego.Router("/participant/:id", &controllers.MainController{}, "get:ViewParticipant")
	beego.Router("/participant/new/survey/:id", &controllers.MainController{}, "get:NewParticipant")
	beego.Router("/participant/create", &controllers.MainController{}, "post:CreateParticipant")
	beego.Router("/participant/results/:id", &controllers.MainController{}, "get:ParticipantResults")

}

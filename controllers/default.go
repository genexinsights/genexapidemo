package controllers

import (
	"encoding/csv"
	"fmt"
	"genexAPIDemo/models"
	"genexAPIDemo/services"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) ConfigError() {
	c.TplName = "configerror.tpl"
}

func (c *MainController) Welcome() {
	c.TplName = "welcome.tpl"
}

func (c *MainController) Get() {
	c.TplName = "index.tpl"

	api := *new(services.Genex)
	s, err := api.GetSurveys()
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	s.SortSurveys()
	c.Data["SurveyList"] = s

}

func (c *MainController) ViewSurvey() {
	c.TplName = "surveys.tpl"

	api := *new(services.Genex)
	surveyIDInput := c.Ctx.Input.Param(":id")

	surveyID, err := strconv.Atoi(surveyIDInput)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	s, err := api.GetSurveyByID(surveyID)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}
	c.Data["Survey"] = s

	// Implement Sorting of Answers based on order field
	for _, question := range s.Survey.Questions {
		question.QuestionGroup.SortAnswers()
	}

}

func (c *MainController) Import() {
	c.TplName = "import.tpl"

	api := *new(services.Genex)
	surveyIDInput := c.Ctx.Input.Param(":id")

	surveyID, err := strconv.Atoi(surveyIDInput)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	s, err := api.GetSurveyByID(surveyID)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}
	c.Data["Survey"] = s

	// Implement Sorting of Answers based on order field
	for _, question := range s.Survey.Questions {
		question.QuestionGroup.SortAnswers()
	}

}

func (c *MainController) ImportUploadFile() {
	c.TplName = "importfileconfig.tpl"

	api := *new(services.Genex)
	surveyIDInput := c.Ctx.Input.Param(":id")
	c.Data["surveyID"] = c.Ctx.Input.Param(":id")
	surveyID, err := strconv.Atoi(surveyIDInput)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	s, err := api.GetSurveyDemographics(surveyID)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}
	c.Data["Demographics"] = s.Demographics

	//fmt.Println(s.Demographics)

	file, header, _ := c.GetFile("the_file") // where <<this>> is the controller and <<file>> the id of your form field
	fileName := ""
	if file != nil {
		// get the filename
		fileName = header.Filename
		fileName = strings.Replace(fileName, " ", "", -1)
		c.Data["FileName"] = fileName
		// save to server
		c.SaveToFile("the_file", "/tmp/"+fileName)

	}
	//fmt.Println("Filename:" + fileName)
	in, err := ioutil.ReadFile("/tmp/" + fileName)
	if err == nil {
		r := csv.NewReader(strings.NewReader(string(in)))

		record, err := r.Read()
		if err != nil {
			log.Fatal(err)
		}
		c.Data["FileHeaders"] = record
	} else {
		fmt.Println("Error Reading CSV File", err)
	}

	//Clean up file
	//os.Remove("/tmp/ + fileName)
}

func (c *MainController) ImportUploadProcessFile() {

	c.TplName = "importfileresult.tpl"

	surveyIDInput := c.Ctx.Input.Param(":id")
	fileName := c.GetString("fileName")

	var importStatus map[int]models.ParticipantImportResponse
	importStatus = make(map[int]models.ParticipantImportResponse)

	surveyID, err := strconv.Atoi(surveyIDInput)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}
	fmt.Println(surveyID)
	batch := false
	batchTotal := 0
	if c.GetString("batch") == "on" {
		batch = true
	}

	api := *new(services.Genex)
	s, err := api.GetSurveyDemographics(surveyID)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}
	c.Data["Demographics"] = s.Demographics

	var demographicsMap map[int]string
	demographicsMap = make(map[int]string)
	for key, val := range c.Ctx.Request.PostForm {
		keysplit := strings.Split(key, "_")
		if keysplit[0] != "header" {
			continue
		}
		i, err := strconv.Atoi(keysplit[1])
		if err != nil {
			c.Data["Errors"] = []string{err.Error()}
			return
		}
		demographicsMap[i] = val[0]
	}

	in, err := ioutil.ReadFile("/tmp/" + fileName)
	if err == nil {

		r := csv.NewReader(strings.NewReader(string(in)))
		rAll, err := r.ReadAll()
		if err != nil {
			log.Fatal(err)
		}
		if batch {
			batchTotal = len(rAll) - 1 //Less one for header
		}

		for recordIndex, record := range rAll {
			if recordIndex == 0 {
				continue
			}
			participant := models.ImportParticipant{SurveyID: surveyID, Batch: batch, BatchName: "", BatchTotal: batchTotal}

			for index, column := range record {
				//fmt.Println(demographicsMap[index], column)
				demographic := models.ParticipantDemographics{}
				intTypeID, err := strconv.Atoi(demographicsMap[index])
				if err != nil {
					continue
				}
				demographic.DemographicsTypeID = intTypeID
				demographic.DemographicsValue = column
				participant.Demographics = append(participant.Demographics, demographic)
			}
			//Submit the particiapnt

			fmt.Println(participant)

			api := *new(services.Genex)
			participantImportResponse, err := api.ImportParticipant(participant)
			if err != nil {
				c.Data["Errors"] = []string{err.Error()}
				continue
			}

			if !participantImportResponse.ResultOK {
				c.Data["Errors"] = participantImportResponse.Errors
				continue
			}
			participantImportResponse.Participant = participant
			fmt.Println("Success", participant)
			importStatus[recordIndex] = participantImportResponse
		}
	} else {
		fmt.Println("Error Reading CSV File", err)
	}
	c.Data["ImportStatus"] = importStatus
	os.Remove("/tmp/" + fileName)
}

func (c *MainController) SubmitSurvey() {

	c.TplName = "submitsurvey.tpl"

	surveyIDInt, err := strconv.Atoi(c.GetString("surveyID"))
	fmt.Println(surveyIDInt)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}
	participantIDInt, err := strconv.Atoi(c.GetString("participantID"))
	fmt.Println(participantIDInt)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}
	c.Data["ParticipantID"] = participantIDInt

	participant := models.ParticipantRepliesBulkRequest{SurveyID: surveyIDInt, ParticipantID: participantIDInt}
	for key, val := range c.Ctx.Request.PostForm {

		//fmt.Println(key)
		keysplit := strings.Split(key, "_")

		//fmt.Println(keysplit)
		if keysplit[0] != "question" {
			continue
		}

		reply := models.ParticipantReply{}
		questionType, _ := strconv.Atoi(keysplit[1])
		questionID, _ := strconv.Atoi(keysplit[2])
		column, _ := strconv.Atoi(keysplit[3])
		group, _ := strconv.Atoi(keysplit[4])

		reply.QuestionID = questionID
		reply.Column = column
		reply.Group = group
		if questionType == 1 {
			answerID, _ := strconv.Atoi(val[0])
			reply.AnswerID = answerID
		} else if questionType == 4 {
			reply.TextAnswer = val[0]
		}
		participant.ParticipantReplies = append(participant.ParticipantReplies, reply)

	}

	api := *new(services.Genex)
	response, err := api.SubmitParticipantReplies(participant)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}
	if !response.ResultOK {
		c.Data["Errors"] = response.Errors
		return
	}

	fmt.Println(response.ResolveCaseID)
	c.Data["ResolveCaseID"] = response.ResolveCaseID

}

func (c *MainController) NewParticipant() {

	c.TplName = "newparticipant.tpl"

	api := *new(services.Genex)
	surveyIDInput := c.Ctx.Input.Param(":id")

	surveyID, err := strconv.Atoi(surveyIDInput)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	s, err := api.GetSurveyDemographics(surveyID)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}
	c.Data["Demographics"] = s.Demographics
	c.Data["surveyID"] = surveyIDInput

}

func (c *MainController) CreateParticipant() {
	c.TplName = "answersurvey.tpl"

	surveyIDInt, err := strconv.Atoi(c.GetString("surveyID"))
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	participant := models.Participant{SurveyID: surveyIDInt}

	for key, val := range c.Ctx.Request.PostForm {

		keysplit := strings.Split(key, "_")
		if keysplit[0] != "demographics" {
			continue
		}
		demographic := models.ParticipantDemographics{}

		intTypeID, err := strconv.Atoi(keysplit[1])
		if err != nil {
			continue
		}
		demographic.DemographicsTypeID = intTypeID
		demographic.DemographicsValue = val[0]

		participant.Demographics = append(participant.Demographics, demographic)

	}

	api := *new(services.Genex)
	participantResponse, err := api.CreateParticipant(participant)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	if !participantResponse.ResultOK {
		c.Data["Errors"] = participantResponse.Errors
		return
	}

	c.Data["ParticipantID"] = participantResponse.ParticipantID
	c.Data["SurveyID"] = surveyIDInt

	s, err := api.GetSurveyByID(surveyIDInt)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	// Implement Sorting of Answers based on order field
	for _, question := range s.Survey.Questions {
		question.QuestionGroup.SortAnswers()
	}

	c.Data["Survey"] = s

}

func (c *MainController) ParticipantResults() {

	c.TplName = "viewresults.tpl"

	participantIDInt, err := strconv.Atoi(c.GetString(":id"))
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	api := *new(services.Genex)
	participantResponse, err := api.ViewParticipantResults(participantIDInt)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	if !participantResponse.ResultOK {
		c.Data["Errors"] = participantResponse.Errors
		return
	}

	s, err := api.GetSurveyByID(participantResponse.SurveyID)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	var questionMap map[int]string
	questionMap = make(map[int]string)
	for _, reply := range participantResponse.Replies {
		text := reply.GetQuestion(s.Survey)
		questionMap[reply.QuestionID] = text
	}

	c.Data["QuestionMap"] = questionMap
	c.Data["Replies"] = participantResponse.Replies
	c.Data["ParticipantID"] = participantIDInt

}

func (c *MainController) ViewParticipant() {

	c.TplName = "viewparticipant.tpl"
	participantIDInt, err := strconv.Atoi(c.GetString(":id"))
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	api := *new(services.Genex)
	p, err := api.ViewParticipant(participantIDInt)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	s, err := api.GetSurveyByID(p.Participant.SurveyID)
	if err != nil {
		c.Data["Errors"] = []string{err.Error()}
		return
	}

	var questionMap map[int]string
	questionMap = make(map[int]string)
	for _, reply := range p.Participant.Replies {
		text := reply.GetQuestion(s.Survey)
		questionMap[reply.QuestionID] = text
	}

	c.Data["Participant"] = p.Participant
	c.Data["QuestionMap"] = questionMap
	c.Data["Replies"] = p.Participant.Replies

}
